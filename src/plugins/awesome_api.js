export default {
  install: (app) => {
    // first time url
    app.config.globalProperties.api_url = 'http://api.home/api/';
    // app.config.globalProperties.api_url = 'http://api.server.home/api/';

    /**
     * input string, object; uri (required), query (optional)
     * return object; apiResult
     */
    app.config.globalProperties.$api = {
      request: async function(uri, query = false, method = 'GET') {
        let url = app.config.globalProperties.api_url + uri

        if(query !== false && method == 'GET') {
          url += '?' + new URLSearchParams(query);
        }

        let options = {
          method: method,
        }

        if(method == 'POST') {
          options['body'] = JSON.stringify(query);
          options['headers'] = {
            'Content-Type': 'application/json',
          }
        }

        const token = localStorage.getItem('token')

        if(token) {
          options['headers'] = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
          }
        }

        const res = await fetch(url, options).catch((err) => {
          console.log('An API error occurred', err)
        })

        if(typeof(res) === 'undefined') {
            throw new Error('Could not get API resource.'); 
        }

        return res.json();
      },

      get: function(uri, query = false) {
        return this.request(uri, query, 'GET')
      },
      
      post: function(uri, query = false) {
        return this.request(uri, query, 'POST')
      }
    }

    // upon INSTALL determine the api domain we should use, the fast tv connected server or the low-energy pi
    app.config.globalProperties.$api.get('devices/get', { type: 'media_server' }).then((device) => {
      if(device.online) {
        // if my media_server is online then obey...
        // app.config.globalProperties.api_url = 'http://' + device.ip + '/api/'
        app.config.globalProperties.api_url = 'http://api.server.home/api/' // temp via hardcoded domain
        console.log('app.config.globalProperties.api_url', app.config.globalProperties.api_url)
      }
    }).catch((err) => {
      console.log('A fallback API error occurred', err)
    })
  }
}