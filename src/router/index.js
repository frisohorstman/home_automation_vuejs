import { createRouter, createWebHistory } from 'vue-router'

// components
import HomePage from '@/components/HomePage.vue'
import MoviesPage from '@/components/MoviesPage.vue'
import VoiceFunctionsPage from '@/components/VoiceFunctionsPage.vue'
import VoiceFunctionCreatePage from '@/components/VoiceFunctionCreatePage.vue'
import StatusPage from '@/components/StatusPage.vue'

// layouts
import DefaultLayout from '@/layouts/DefaultLayout.vue'
import LoginLayout from '@/layouts/LoginLayout.vue'

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage,
    meta: { layout: DefaultLayout }
  },
  {
    path: '/movies',
    name: 'MoviesPage',
    component: MoviesPage,
    meta: { layout: DefaultLayout }
  },
  {
    path: '/voice/functions',
    name: 'VoiceFunctionsPage',
    component: VoiceFunctionsPage,
    meta: { layout: DefaultLayout }
  },
  {
    path: '/voice/functions/create',
    name: 'VoiceFunctionCreatePage',
    component: VoiceFunctionCreatePage,
    meta: { layout: DefaultLayout }
  },
  {
    path: '/status',
    name: 'StatusPage',
    component: StatusPage,
    meta: { layout: DefaultLayout }
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: VoiceFunctionsPage,
    meta: { layout: LoginLayout }
  }
]

const router = createRouter({
  mode: 'history',
  fallback: true,
  history: createWebHistory(),
  base: '/',
  routes
})

export default router
