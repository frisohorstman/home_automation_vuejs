import { createApp } from 'vue'
import App from './App.vue';
import router from './router'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { faServer } from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(faServer)

// plugins
import awesomeApi from './plugins/awesome_api.js'


const app = createApp(App)
app.component('font-awesome-icon', FontAwesomeIcon)
app.use(router)
app.use(awesomeApi)
app.mount('#app')